FROM nginxinc/nginx-unprivileged:1-alpine

LABEL maintainer="lforster@hotmail.co.uk"

COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params

ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

USER root

run mkdir -p /vol/static
run chmod 755 /vol/static
run touch /etc/nginx/conf.d/default.conf
run chown nginx:nginx /etc/nginx/conf.d/default.conf

COPY ./entrypoint.sh /entrypoint.sh
run chmod +x /entrypoint.sh

user nginx

cmd ["/entrypoint.sh"]